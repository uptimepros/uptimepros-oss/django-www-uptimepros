"""www_uptimepros URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from django.views.generic.base import TemplateView, RedirectView
from pages import urls

urlpatterns = [
    path('favicon.ico', RedirectView.as_view(url='static/images/favicon.ico', permanent=True)),
    path('admin/', admin.site.urls),
    path('services', TemplateView.as_view(template_name='services.html'), name='services'),
    path('about', TemplateView.as_view(template_name='about.html'), name='about'),
    path('testimonials', TemplateView.as_view(template_name='testimonials.html'), name='testimonials'),
    path('careers', TemplateView.as_view(template_name='careers.html'), name='careers'),
    path('blog', TemplateView.as_view(template_name='blog.html'), name='blog'),
    path('', TemplateView.as_view(template_name='home.html'), name='home'),
]